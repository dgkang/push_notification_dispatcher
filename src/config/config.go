package config

import (
    "crypto/tls"
    "encoding/json"
    "io/ioutil"
)

/*
 * Config: exported struct
 * CONFIG: exported global var
 */

type (
    TLSConfig struct {
        TLSConfig *tls.Config
    }
    Config struct {
        LogFile string `json:"log_file"`
        LogLevel string `json:"log_level"`
        PidFile string `json:"pid_file"`
        Redis struct {
            Host string `json:"host"`
            Timeout float64 `json:"timeout"`
        } `json:"redis"`
        IOS []struct {
            AppName string `json:"app_name"`
            SandBox struct {
                CertFile string `json:"cert_file"`
                KeyFile string `json:"key_file"`
            } `json:"sandbox"`
            Production struct {
                CertFile string `json:"cert_file"`
                KeyFile string `json:"key_file"`
            } `json:"production"`
        } `json:"ios"`

        IOSPush map[string]map[string]TLSConfig
    }
)

var (
    CONFIG Config
)

func ConfigReload(path string) error {
    content, err := ioutil.ReadFile(path)
    if err != nil {
        return err
    }

    var tmp Config
    tmp.IOSPush = make(map[string]map[string]TLSConfig)// init map outside
    if err = json.Unmarshal(content, &tmp); err != nil {
        return err
    }

    for _, v := range tmp.IOS {
        cert_sandbox, err := tls.LoadX509KeyPair(v.SandBox.CertFile, v.SandBox.KeyFile)
        if err != nil {
            return err
        }

        cert_production, err := tls.LoadX509KeyPair(v.Production.CertFile, v.Production.KeyFile)
        if err != nil {
            return err
        }

        tmp.IOSPush[v.AppName] = make(map[string]TLSConfig)// init map inside
        tmp.IOSPush[v.AppName] = map[string]TLSConfig {
            "sandbox": {
                & tls.Config {
                    InsecureSkipVerify: true,
                    Certificates: []tls.Certificate{cert_sandbox},
                },
            },
            "production": {
                & tls.Config {
                    InsecureSkipVerify: true,
                    Certificates: []tls.Certificate{cert_production},
                },
            },
        }

    }
    CONFIG = tmp
    return nil
}

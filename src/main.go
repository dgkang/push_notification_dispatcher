package main

import (
	"bytes"
	"crypto/tls"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/fzzy/radix/redis"

	"config"
	"logger"
)

var (
	flag_config = flag.String("config", "", "config file path")

	redis_channel_number              = runtime.NumCPU()
	redis_channel                     = make(chan *redis.Client, redis_channel_number)
	redis_key_event_push_notification = "event.push_notification" //right push and left pop

	chan_push_notification = make(chan string, 1024) // buffered queue for push notification

	chan_apns_notification_id = make(chan uint32, 1) //generate notification id for every push notification
	apns                      = map[string]struct {
		Host    string
		Chan    chan *tls.Conn
		ChanNum int
	}{
		"sandbox": {
			"gateway.sandbox.push.apple.com:2195",
			make(chan *tls.Conn, runtime.NumCPU()*2),
			runtime.NumCPU() * 2,
		},
		"production": {
			"gateway.push.apple.com:2195",
			make(chan *tls.Conn, runtime.NumCPU()*2),
			runtime.NumCPU() * 2,
		},
	}
)

func main() {
	defer logger.LogClose()

	// init terminal args
	flag.Parse()
	if *flag_config == "" {
		flag.Usage()
		return
	}

	// init rand seed
	rand.Seed(int64(os.Getpid()) ^ time.Now().Unix())

	// init chan (apns notification id)
	chan_apns_notification_id <- 0

	// init tls pool via channel
	for i := 0; i < apns["sandbox"].ChanNum; i++ {
		apns["sandbox"].Chan <- nil
	}
	for i := 0; i < apns["production"].ChanNum; i++ {
		apns["production"].Chan <- nil
	}

	// init redis client pool via channel
	for i := 0; i < redis_channel_number; i++ {
		redis_channel <- nil
	}

	// init config
	var err error
	err = config.ConfigReload(*flag_config)
	if err != nil {
		logger.Log(logger.LOG_LEVEL_ERROR, err)
		return
	}
	logger.Log(logger.LOG_LEVEL_DEBUG, config.CONFIG)

	// init log
	err = logger.LogReload(config.CONFIG.LogFile, config.CONFIG.LogLevel)
	if err != nil {
		logger.Log(logger.LOG_LEVEL_ERROR, err)
		return
	}

	// write pid
	if err = writePIDFile(); err != nil {
		logger.Log(logger.LOG_LEVEL_ERROR, err)
		return
	}

	// get push notification and push into chan_push_notification
	logger.Log(logger.LOG_LEVEL_INFO, "push notification dispatcher start...")
	for i := 0; i < runtime.NumCPU(); i++ {
		go func() {
			getPushNotification()
		}()
	}

	// get push notification from chan_push_notification and really push them to user device
	for i := 0; i < runtime.NumCPU()*10; i++ {
		go func() {
			dispatchPushNotification()
		}()
	}
	handleSignal()
}

func getAPNSNotificationID() uint32 { // must atomic
	n := <-chan_apns_notification_id
	n++
	chan_apns_notification_id <- n
	return n
}

func handleSignal() {
	signal_chan := make(chan os.Signal, 1)
	signal.Notify(signal_chan, []os.Signal{syscall.SIGHUP, syscall.SIGUSR1}...)
	for {
		s := <-signal_chan
		logger.Log(logger.LOG_LEVEL_INFO, "signal:", s)

		switch s {
		case syscall.SIGPIPE:
		case syscall.SIGHUP:
			if err := config.ConfigReload(*flag_config); err != nil {
				logger.Log(logger.LOG_LEVEL_ERROR, err)
			} else {
				logger.Log(logger.LOG_LEVEL_INFO, "reload config success")
			}
		case syscall.SIGTERM:
			logger.Log(logger.LOG_LEVEL_WARNING, "push notification stop...")
			return
		case syscall.SIGINT:
		case syscall.SIGUSR1:
			if err := logger.LogReload(config.CONFIG.LogFile, config.CONFIG.LogLevel); err != nil {
				logger.Log(logger.LOG_LEVEL_ERROR, err)
			} else {
				logger.Log(logger.LOG_LEVEL_INFO, "reload log success")
			}
		case syscall.SIGUSR2:
		default:
		}
	}
}

func writePIDFile() error {
	pid := fmt.Sprintf("%d", os.Getpid())
	return ioutil.WriteFile(config.CONFIG.PidFile, []byte(pid), 0644)
}

func redisCommand(cmd string, args ...interface{}) *redis.Reply {
	client := <-redis_channel
	defer func() {
		redis_channel <- client
	}()

	if client == nil {
		var err error
		client, err = redis.DialTimeout("tcp", config.CONFIG.Redis.Host, time.Duration(config.CONFIG.Redis.Timeout)*time.Millisecond)
		if err != nil {
			logger.Log(logger.LOG_LEVEL_ERROR, err)
			return nil
		}
	}

	reply := client.Cmd(cmd, args...)
	if reply.Err != nil {
		logger.Log(logger.LOG_LEVEL_ERROR, reply.Err)
		client.Close() // already called in Cmd() if reply error, close again for safe
		client = nil
		return nil
	}
	return reply
}

func bwrite(w io.Writer, values ...interface{}) (err error) {
	for _, v := range values {
		if err := binary.Write(w, binary.BigEndian, v); err != nil {
			return err
		}
	}
	return nil
}

func dispatchPushNotification() {
	msg := <-chan_push_notification
	notification_id := getAPNSNotificationID()

	var err error
	defer func() {
		if err != nil {
			logger.Log(logger.LOG_LEVEL_ERROR, fmt.Sprintf("dispatch push notification, err = %v, id = %d, msg = %s", err, notification_id, msg))
		} else {
			logger.Log(logger.LOG_LEVEL_INFO, fmt.Sprintf("dispatch push notification, id = %d, msg = %s", notification_id, msg))
		}
	}()

	// check validity of the fields
	type Data struct {
		PlatForm    string `json:"platform"`
		Enviroment  string `json:"enviroment"`
		AppName     string `json:"app_name"`
		DeviceToken string `json:"device_token"`
		Payload     struct {
			Aps struct {
				Alert string  `json:"alert"`
				Badge float64 `json:"badge"`
				Sound string  `json:"sound"`
			} `json:"aps"`
		} `json:"payload"`
	}
	var data Data
	err = json.Unmarshal([]byte(msg), &data)
	if err != nil {
		return
	}

	payload, err := json.Marshal(data.Payload)
	if err != nil {
		return
	}

	if (data.PlatForm != "ios") ||
		(data.Enviroment != "sandbox" && data.Enviroment != "production") ||
		(len(data.DeviceToken) != 64) ||
		(len(payload) > 256) {
		err = errors.New("data format wrong")
		return
	}

	// get tls connection
	client := <-apns[data.Enviroment].Chan
	defer func() {
		apns[data.Enviroment].Chan <- client
	}()
	if client == nil {
		client, err = tls.Dial("tcp", apns[data.Enviroment].Host, config.CONFIG.IOSPush[data.AppName][data.Enviroment].TLSConfig)
		if err != nil { // if err occur, returned client is nil
			return
		}
	}

	// assemble and send
	byte_device_token, err := hex.DecodeString(data.DeviceToken)
	if err != nil {
		return
	}
	buffer := bytes.NewBuffer([]byte{})
	frame_len := 3 + len(byte_device_token) + 3 + len(payload) + 3 + 4
	if err = bwrite(
		buffer,
		uint8(2),
		uint32(frame_len),
		uint8(1),
		uint16(len(byte_device_token)),
		byte_device_token,
		uint8(2),
		uint16(len(payload)),
		payload,
		uint8(3),
		uint16(4),
		notification_id,
	); err != nil {
		return
	}

	if _, err = client.Write(buffer.Bytes()); err != nil {
		client.Close()
		client = nil
		return
	}

	client.SetReadDeadline(time.Now().Add(10 * time.Millisecond)) // read timeout 10ms
	readed := [6]byte{}
	n, err := client.Read(readed[:])
	if err != nil {
		if op_err, ok := err.(*net.OpError); ok && op_err.Timeout() { //if err is timeout then do nothing
			logger.Log(logger.LOG_LEVEL_DEBUG, "timeout, it's ok")
			err = nil
		} else {
			client.Close()
			client = nil
		}
		return
	}

	if n > 1 {
		var status uint8 = uint8(readed[1])
		switch status {
		case 0:
			logger.Log(logger.LOG_LEVEL_WARNING, "successed, returned status code = 0")
			return
		default:
			client.Close()
			client = nil

			var readed_notification_id uint32
			binary.Read(bytes.NewBuffer(readed[2:]), binary.BigEndian, &readed_notification_id)
			err = errors.New(fmt.Sprintf("err code = %d, nofitication id = %d", status, readed_notification_id))
			return
		}
	}
}

func getPushNotification() {
	var last_sleep_init time.Duration = 50 * time.Millisecond
	var last_sleep_for_connection_error time.Duration = last_sleep_init
	var last_sleep_for_no_element_in_redis time.Duration = last_sleep_init
	for {
		reply := redisCommand("lpop", redis_key_event_push_notification)
		if reply == nil { // if can't connect, then sleep and retry
			logger.Log(logger.LOG_LEVEL_ERROR, "sleep for connection error", last_sleep_for_connection_error)
			time.Sleep(last_sleep_for_connection_error)
			last_sleep_for_connection_error *= 2
			if last_sleep_for_connection_error >= 10*time.Second {
				last_sleep_for_connection_error = 10 * time.Second
			}
			continue
		}
		last_sleep_for_connection_error = last_sleep_init

		// push into channel
		if str, err := reply.Str(); err == nil {
			chan_push_notification <- str
			last_sleep_for_no_element_in_redis = last_sleep_init
		} else {
			time.Sleep(last_sleep_for_no_element_in_redis)
			last_sleep_for_no_element_in_redis *= 2
			if last_sleep_for_no_element_in_redis >= 2*time.Second {
				last_sleep_for_no_element_in_redis = 2 * time.Second
			}
			if reply.Type == redis.NilReply { // no element in redis
			} else { // other condition, should not happen
				logger.Log(logger.LOG_LEVEL_ERROR, err)
			}
			continue
		}
	}
}
